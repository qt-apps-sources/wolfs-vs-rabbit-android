#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QQuickWidget(parent)
{
    setSource(QUrl("qrc:/MainWindow.qml"));
    setResizeMode(QQuickWidget::SizeRootObjectToView);


    step_states = select_figure;

    connect(rootObject(), SIGNAL(on_cell_clicked(int, int)),
            this, SLOT(on_cell_clicked(int, int)));

    connect(rootObject(), SIGNAL(reset_game()), this, SLOT(reset_game()));
    connect(rootObject(), SIGNAL(changed_player(int)), this, SLOT(set_player(int)));
    connect(rootObject(), SIGNAL(set_difficult(int)), this, SLOT(set_difficult(int)));

    game = new Game;
    game->setPlayMode(Game::MT_RABBIT);
    game->setAILevel(1);
    reset_game();
}

MainWindow::~MainWindow()
{

}

void MainWindow::init_field()
{
    for(int i = 0; i < COLUMNS_NUMBER; i++)
        for(int j = 0; j < ROWS_NUMBER; j++)
            game_field[i][j] = empty;

    update_field();
}

void MainWindow::update_field()
{
    // Set wolfs
    for(int i = 0; i < 4; i++)
    {
        game_field[game->wolfs[i].y()][game->wolfs[i].x()] = wolf;
    }
    game_field[game->rabbit.y()][game->rabbit.x()] = rabbit;

    for(int i = 0; i < COLUMNS_NUMBER; i++)
        for(int j = 0; j < ROWS_NUMBER; j++)
            QMetaObject::invokeMethod(rootObject(), "change_field_element",
                                                      Q_ARG(QVariant, i),
                                                      Q_ARG(QVariant, j),
                                                      Q_ARG(QVariant, game_field[i][j]));
}

void MainWindow::reset_game()
{
    qDebug() << "Reset game";
    step_states = select_figure;
    game->reset();
    QMetaObject::invokeMethod(rootObject(), "set_message", Q_ARG(QVariant, ""));
    init_field();
}

void MainWindow::set_difficult(int value)
{
    qDebug() << "Difficult set " << value;
    game->setAILevel(value);
    reset_game();
}

void MainWindow::set_player(int id)
{
    if(id)
        game->setPlayMode(Game::MT_WOLF);
    else
        game->setPlayMode(Game::MT_RABBIT);

    reset_game();
}

void MainWindow::on_cell_clicked(int column, int row)
{
    qDebug() << column << " " << row;
    switch (step_states)
    {
        case select_figure:
        {
            if (!this->game->isActive() || !this->game->isPlayersTurn())
                    return;
                int monsterIndex = game->getMonsterIndexOnPosition(QPoint(row, column));
            if (monsterIndex < 0)
                    return;
            if (this->game->getMonsterType(monsterIndex) != this->game->getPlayMode())
                return;

            this->game->setSelectedMonsterIndex(monsterIndex);
            step_states = select_place;
            break;
        }
        case select_place:
        {
            bool available_step = false;
            this->game->moveSelectedMonsterToPosition(QPoint(row, column));
            this->game->setSelectedMonsterIndex(-1);
            Game::MonsterType winner;
            if (this->game->isGameOver(winner))
            {
                if(winner == game->getPlayMode())
                   QMetaObject::invokeMethod(rootObject(), "set_message", Q_ARG(QVariant, "You win!"));
                else
                    QMetaObject::invokeMethod(rootObject(), "set_message", Q_ARG(QVariant, "Game over!"));

            }

            step_states = select_figure;
            init_field();
        }
    }
}

// Check available steps

QList<Cell_coordinate> MainWindow::get_available_rabbits_steps(int column, int row)
{
    QList<Cell_coordinate> result;
    // Add all positions
    result.append({column - 1, row - 1});
    result.append({column + 1, row + 1});
    result.append({column - 1, row + 1});
    result.append({column + 1, row - 1});
    // Remove positions, which not available
    int i = 0;
    foreach(Cell_coordinate cell, result)
    {
        int column = cell.column;
        int row = cell.row;
        // Check borders
        if(row < 0 || column < 0 || column >= COLUMNS_NUMBER || row >= ROWS_NUMBER)
        {
            result.removeAt(i);
            continue;
        }
        // Check any players
        if(game_field[column][row] != empty)
        {
            result.removeAt(i);
            continue;
        }
        i++;
    }
    return result;
}

QList<Cell_coordinate> MainWindow::get_available_wolf_steps(int column, int row)
{
    QList<Cell_coordinate> result;
    // Add all positions
    result.append({column + 1, row + 1});
    result.append({column + 1, row - 1});
    // Remove positions, which not available
    int i = 0;
    foreach(Cell_coordinate cell, result)
    {
        int column = cell.column;
        int row = cell.row;
        // Check borders
        if(row < 0 || column < 0 || column >= COLUMNS_NUMBER || row >= ROWS_NUMBER)
        {
            result.removeAt(i);
            continue;
        }
        // Check any players
        if(game_field[column][row] != empty)
        {
            result.removeAt(i);
            continue;
        }
        i++;
    }

    return result;
}

bool MainWindow::contains_cell(QList<Cell_coordinate> list, Cell_coordinate coordinate)
{
    for(int i = 0; i < list.length(); i++)
    {
        int column = list.at(i).column;
        int row = list.at(i).row;

        if(coordinate.row == row && coordinate.column == column)
            return true;
    }
    return false;
}
