import QtQuick 2.0
import QtQuick.Controls 2.5

import "qrc:/qml_primitives/"

Item {
    //Propertys
    id: root
    height:  1920
    width: 1080

    // Signals
    signal on_cell_clicked(int column, int row);
    signal changed_player(int id);
    signal reset_game();
    signal set_difficult(int value);

    // Functions
    function change_field_element(column, row, value)
    {
        if(value >= 0 && value < 3)
            if(column >= 0 && column < 8 &&
                    row >= 0 && row < 8)
                game_field.set_player(column, row, value);
            else
                console.log("Wrong coordinate: " + String(column) + " " + String (row));
        else
            console.log("Wrong value " + String(value));
    }

    function set_message(value)
    {
        message.text = value;
    }

    // Elements
    Rectangle
    {
        id: background
        anchors.fill: parent
        color: "#292929"
    }

    Field
    {
        id: game_field
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Button
    {
        id: reset_button
        height: 50
        width: 300
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Reset game"

    }

    ComboBox
    {
        id: player_combobox;
        height: 50
        width: 300
        y: reset_button.y
        x: reset_button.x + reset_button.width + 50
        spacing: 50
        model: ["Rabbit", "Wolf"]

        property var previous_index: -1
        down:
        {
            if(player_combobox.currentIndex != previous_index && currentIndex != -1)
            {
                changed_player(player_combobox.currentIndex);
            }

        }
    }

    Column
    {
        id: mode_column
        x: reset_button.x - 300
        spacing: 10

        Text
        {
            text: "Difficult"
            color: "#ffffff"
            font.pixelSize: 40

        }
        RadioButton
        {
            id: easy_button
            checked: true

            Text
            {
                x: 100
                y: 10
                text: "Easy"
                color: "#ffffff"
                font.pixelSize: 32
            }
            onClicked: set_difficult(1);

        }
        RadioButton
        {
            Text
            {
                x: 100
                y: 10
                color: "#ffffff"
                text: "Medium"
                font.pixelSize: 32
            }
            onClicked: set_difficult(2);
        }
        RadioButton
        {
            Text
            {
                x: 100
                y: 10
                color: "#ffffff"
                text: "Hard"
                font.pixelSize: 32
            }
            onClicked: set_difficult(3);
        }
    }

    Text
    {
        id: message
        text: ""
        font.pixelSize: 60
        font.bold: true
        color: "#ff0000"

        y: reset_button.y + 300
        anchors.horizontalCenter: parent.horizontalCenter

    }


    // Initialization
    Component.onCompleted:
    {
        game_field.clicked_on_cell.connect(root.on_cell_clicked);
        reset_button.clicked.connect(root.reset_game);

    }
}
