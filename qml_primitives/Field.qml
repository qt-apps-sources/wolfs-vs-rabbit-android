import QtQuick 2.0

import "qrc:/qml_primitives/"


Item
{
    id: root

    height: grid_view.height
    width: grid_view.width

    property var columns_number: 8
    property var rows_number: 8

    signal clicked_on_cell(int column, int row)

    GridView
    {
        id: grid_view

        cellWidth: 120
        cellHeight: 120

        width: columns_number * cellWidth
        height:  rows_number * cellHeight

        interactive: false
        model: dataModel
        flow: GridView.FlowLeftToRight

        delegate: Cell
        {
            column: model.column
            row: model.row
            player_color: model.player_color

            Component.onCompleted:
            {
                clicked.connect(root.clicked_on_cell)
            }
        }

        Component.onCompleted:
        {
            for(let i = 0; i < columns_number * rows_number; i++)
                dataModel.append({column: Math.floor(i / rows_number), row: Math.floor(i % rows_number)});
        }

        ListModel
        {
            id: dataModel
        }
    }


    function set_player(column, row, player)
    {
        var color;

        switch(player)
        {
        case 1:
            color = "#00ff00";    // rabbit
            break;
        case 2:
            color = "#ff0000";    // wolf
            break;
         default:
             color = "transparent"
        }

        dataModel.setProperty(column * rows_number + row, "player_color", color);
    }

}
