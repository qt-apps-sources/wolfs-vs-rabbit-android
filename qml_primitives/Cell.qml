import QtQuick 2.0

/*
 *  0 - empty
 *  1 - rabbit
 *  2 - wolf
 */

Item
{
    id: root

    height: 119
    width: 119

    property var column: 0
    property var row: 0
    property var player_color: "transparent"


    signal clicked(int column, int row)


    MouseArea
    {
        anchors.fill: parent

        Rectangle
        {
            id: background
            anchors.fill: parent
            color: (column + row) % 2 == 0? "#000000" : "#ffffff"
        }

        onClicked:
        {
            root.clicked(column, row);
        }

        Rectangle
        {
            id: player_circle
            anchors.centerIn: parent
            width: 100
            height: 100
            radius: width / 2
            color: player_color
        }

    }

    function set_player(player)
    {

    }
}
