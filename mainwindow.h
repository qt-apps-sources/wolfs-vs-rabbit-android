#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QQuickWidget>
#include <QQuickItem>
#include <QMetaObject>
#include <QDebug>

#include "game/game.h"

#define COLUMNS_NUMBER 8
#define ROWS_NUMBER 8


enum Cell_modes
{
    empty = 0,
    rabbit,
    wolf
};

enum Step_states
{
    select_figure = 0,
    select_place
};

struct Cell_coordinate
{
    int column;
    int row;
};

class MainWindow : public QQuickWidget
{
    Q_OBJECT
private:
    Cell_modes game_field [COLUMNS_NUMBER][ROWS_NUMBER];

    // Step
    Step_states step_states;
    int selected_column;
    int selected_row;

    Cell_modes current_player;

    void init_field();
    void update_field();
    QList<Cell_coordinate> get_available_rabbits_steps(int column, int row);
    QList<Cell_coordinate> get_available_wolf_steps(int column, int row);
    bool contains_cell(QList<Cell_coordinate> list, Cell_coordinate coordinate);

    Game *game;

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_cell_clicked(int column, int row);
    void reset_game();
    void set_player(int);
    void set_difficult(int);

signals:
    void wrong_step(int column, int row);
    void rabbit_lose();
    void send_win_message(QString);
};

#endif // MAINWINDOW_H
